@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,700');

html, body {
    padding: 0;
    margin: 0;
}

a {
	display: inline-block;
}

* {
    font-family: Roboto, Georgia, sans-serif;
}

.container {
	weight: 1200px;
	margin: 0 auto;
}





#primary-menu {
    background: #627a2f; 
    height: 75px;   
}

#primary-menu > ul{
    padding: 0;
    margin: 0;
    font-size: 0;
    

}

#primary-menu > ul > li{
    display: inline-block;
    list-style: none;
	font-size: 16px; 
}

#primary-menu > ul > li a {
    color: #ded1f5;
    text-decoration: none;
    height: 75px;
    line-height: 75px;
    padding: 0 15px;  

}

#primary-menu > ul > li a:hover {
    background-color:#ded1f5;
    color: #894a89;
}